<!DOCTYPE html>
<html>
<head>
	<title>Unic Lifestyle</title>
	<link rel="stylesheet" type="text/css" href="slider.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
		<div class="hero">
			<label>
<input type='checkbox'>
<span class='menu'> <span class='hamburger'></span> </span>
<ul>
  <li> <a href='#'>Home</a> </li>
  <li> <a href='#'>Contact Us</a> </li>
  <li> <a href='#'>Categories</a> </li>
  <li> <a href='#'>Enquiery</a> </li>
  <li> <a href='#'>About Us</a> </li>
</ul>
</label>
			<div class="secimg">
				<div class="logo">
				<h1>
				<span class="colchange">Unic</span> Lifestyle	
				</h1>
				</div>
				<div class="cont fadeInUp animated">
					<h2>Style is a way to say <br> <span class="colchange1">who you are</span> <br> without having to speak.</h2>
					<button class="explore">Explore More &nbsp <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
				</div>
				<div class="cont1">
					<span class="spanslide">Stop</span> <span class="colchange2">wishing</span>,<br>
						</div>
                    <div class="cont2">
					<span class="spanslide">Start</span> <span class="colchange2">living</span>
				</div>
				</div>
			</div>
</body>
</html>